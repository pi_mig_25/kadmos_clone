This document describes the installation steps to install TiXI on a mac OSx for use in Python.

1) Follow instruction on: https://github.com/DLR-SC/tixi/wiki/InstallationHowto
    - Install CMake
    - Install required packages with homebrew

2) Open CMake and set target directory for build.

3) Set XCode as the compiler.

4) In CMake, press Configure.

5) In CMake, press Generate.

6) In the target directory, create a folder 'lib'.

7) In CMake, press Open Project. The project will now open in XCode.

8) In XCode, go to TIXI/Sources/ALL_BUILD/CMakeLists.txt

9) Press the play button. It should then say: Build succeeded.

10) As a final step you need to put the .dylib files in your lib folder.
    - The four files should be in <target directory>/lib/Debug
    - Copy the files to your local lib folder: usr/local/lib

11) Now you can use the tixiwrapper.py file to use TiXI inside Python. For this, you need to copy the tixiwrapper.py
file to the Python site-packages or a local packages folder.
